package org.bitbucket.anuarkaliyev23.javalin.example;

import io.javalin.Javalin;

public class JavalinMethodsExample {
    public static void main(String[] args) {
        Javalin app = Javalin.create();

        app.get("/", ctx -> ctx.result("Hello from get"));
        app.post("/", ctx -> ctx.result("Hello from post"));
        app.patch("/", ctx -> ctx.result("Hello from patch"));
        app.delete("/", ctx -> ctx.result("Hello from delete"));

        app.start(8080);
    }
}
