package org.bitbucket.anuarkaliyev23.javalin.example;

import io.javalin.Javalin;

public class JavalinParams {
    public static void main(String[] args) {
        Javalin app = Javalin.create();
        app.get("/hello", context -> {
            context.result("Hello, World!");
        });

        app.get("/hello/:name", context -> {
            String name = context.pathParam("name");
            context.result("Hello, " + name);
        });
        app.start(8080);
    }
}
