package org.bitbucket.anuarkaliyev23.javalin.example;

import io.javalin.Javalin;
import io.javalin.http.Context;

public class HelloWorld {
    public static void main(String[] args) {
        Javalin app = Javalin.create();
//        app.get("/", Main::helloWorld);
//        app.get("/", context -> {helloWorld(context);});

        app.get("/", context -> {
            System.out.println("Headers from client:");
            System.out.println(context.headerMap());
            System.out.println("Method from client: ");
            System.out.println(context.method());

            context.result("Hello, Http from lambda!");
        });

        app.get("/something", context -> {
            context.result("Hello, From something!");
        });


        app.start(8080);
    }

//    //Context - HTTP!!!
    public static void helloWorld(Context context) {
        System.out.println(context.headerMap());
        context.result("Hello, HTTP!");
    }
}
